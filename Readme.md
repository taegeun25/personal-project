## SNS 구현 프로젝트
### -박태근 개인 프로젝트-

### [Swagger]
http://ec2-43-200-164-132.ap-northeast-2.compute.amazonaws.com:8080/swagger-ui/index.html

---
### 프로젝트 기술스택
* 에디터 : IntelliJ Ultimate
* 개발툴 : SpringBoot 2.7.5
* 자바 : JAVA 11
* 빌드 : Gradle 7.5.1
* 서버 : AWS EC2 t3-small
* 배포 : Docker
* CiCD: Gitlab
* 데이터베이스 : Mysql 8.0
* Swagger : springdoc-openapi-ui:1.6.14
* 필수라이브러리 : SpringBoot Web, Spring Security,Lombok, JWT, Swagger, Mysql
---  
### 기능요구사항
- [x] Gitlab CI&CD
- [x] Swagger 적용
- [x] AWS EC2 배포
- [x] 회원가입기능 / 테스트코드
- [x] 로그인기능  / 테스트코드
- [x] 게시글 CRUD / 테스트 코드
- [x] 댓글 CRUD / 테스트코드
- [x] 좋아요 등록 / 갯수출력
- [x] 알람 등록, 알람 리스트
- [x] 로그인한 유저 자신의 게시글 모아보기
---
### 프로젝트 ERD
![img_1.png](img_1.png)
---
### EndPoint
![img_2.png](img_2.png)

---
### 프로젝트 소감
#### 첫번째 미션 :
* 프로젝트를 통해 Test코드의 편리함을 많이 느낄 수 있었다. 사실 이번 미션에서는 기능구현을하고
  테스트 코드를 작성해서, 기능을 구현할때에는 swagger로 테스트 하느라 시간이 오래 걸렸었다. 이후
  미션 수행 후 리펙토링을 할 때에는 swagger로 테스트하지 않고 테스트코드로만 진행해보았는데
  시간이 크게 단축되었다.

* 자동 배포의 편리함을 느낄 수 있었다. Gitlab에서 파이프라인을 구축해 CI/CD를 적용하고 프로젝트를
  진행했다. 배포스크립트를 작성하고, 자동화시키는데 어려움을 겪었지만, 커밋할때마다 자동으로 배포가 되어
  시간을 많이 아낄 수 있었다.

#### 두번째 미션 :
* 소프트 딜리트를 적용해 삭제기능을 만들었다. 백엔드에서는 데이터를 관리하기 때문에 삭제할 때 모든 정보를 날려버리지 않고,
  기록해둔다고한다. 이는 법적으로도 정해져있다는 것을 알게되었다.   
  @SQLDelete로 Delete SQl문이 사용될 때 delete_at에 삭제날짜가
  기록되게 하고, @Where를 통해 다른 코드에서 삭제된 내용을 조회할시 delete_at에 값이 존재하면 조회가 불가하게 만들었다.

* Swagger UI상에서 하나의 Controller에 구현되어있는 엔드포인트들을 기능별로 그룹화 하고싶어서,
  springfox라이브러리를 springdoc라이브러리로 바꾸었다.
  기존에는 강사님이 주신 Config 내용을 그대로 복사해서 사용했는데, 라이브러리를 바꾸면서 Header에 jwt토큰을 넣는 로직까지
  내가 직접 만들었더니 API 문서화에 대한 이해도를 높일 수 있었던 것 같다. 같은 고민을 했던 팀원분에게도 도움이 될 수 있었다.
  <blog: https://ggomhi.tistory.com/15>

* Optional사용 주의사항을 참고한 리펙토링, 값있다면 가져오기 위해 .ispresent, .get의 조합을 많이 사용하였다. ispresent를 사용하기 위해
  if문을 남용하다가, 리팩토링이 필요하겠다 싶어 Optional의 주의사항에 대해 알아보다 Optional의 .get은 많은 에러를
  유발 할 가능성이 있기 때문에 지양된다는 걸 알게되었다. <blog: https://ggomhi.tistory.com/14>

### 보완하고 싶은 점
* 조원분의 테스트 코드를 보니, jacoco 코드 커버리지를 사용하셨다, 항상의문이였던 내가 테스트 코드를 잘 작성하고 있는것인지에
  대한 의문이 해결될 것 같아, jacoco 코드 커버리지로 현재상태를 비교하고 개선 해보고 싶다.
* User 권한 기능을 추가하지 못한것이 아쉬움이 남는다. 


