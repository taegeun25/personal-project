package com.likelion.semifinal.service;

import org.springframework.stereotype.Service;

@Service
public class HelloService {

    public Integer sumOfDigits(Integer num) {

        String str = Integer.toString(num);
        int[] nums = new int[str.length()];

        for (int i = 0; i <nums.length; i++) {
            nums[nums.length -i-1] = num % 10;
            num = num / 10;
        }
        int sum = 0;
        for (int i = 0; i < nums.length; i++) {
            sum += nums[i];
        }
        return sum;
    }
}
