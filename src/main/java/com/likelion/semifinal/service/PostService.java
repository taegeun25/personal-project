package com.likelion.semifinal.service;

import com.likelion.semifinal.domain.dto.post.myfeed.FeedResponse;
import com.likelion.semifinal.domain.dto.alarm.AlarmType;
import com.likelion.semifinal.domain.dto.comment.CommentDto;
import com.likelion.semifinal.domain.dto.post.LikeDto;
import com.likelion.semifinal.domain.dto.comment.CommentCreateRequest;
import com.likelion.semifinal.domain.dto.comment.CommentListResponse;
import com.likelion.semifinal.domain.dto.comment.CommentModifyRequest;
import com.likelion.semifinal.domain.dto.post.write.PostCreateRequest;
import com.likelion.semifinal.domain.dto.post.PostDto;
import com.likelion.semifinal.domain.dto.post.modify.PostModifyRequest;
import com.likelion.semifinal.domain.entity.*;
import com.likelion.semifinal.exception.AppException;
import com.likelion.semifinal.exception.ErrorCode;
import com.likelion.semifinal.repository.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
@Slf4j
public class PostService {

    private final PostRepository postRepository;
    private final UserRepository userRepository;
    private final CommentRepository commentRepository;
    private final LikeRepository likeRepository;
    private final AlarmRepository alarmRepository;


// --------------------------------------------------게시글--------------------------------------------------------------

    /**
     * 게시글 작성
     */
    public PostDto writePost(PostCreateRequest request, String userName) {

        //JWT Token에 들어있는 userName이 userRepository에 있는지 확인
        User user = userRepository.findByUserName(userName)
                .orElseThrow(() -> new AppException(ErrorCode.USERNAME_NOT_FOUND));

        Post post = Post.builder()
                .title(request.getTitle())
                .body(request.getBody())
                .user(user)
                .build();

        Post savedPost = postRepository.save(post);

        return PostDto.of(savedPost, userName);
    }

    /**
     * 게시글 단건 조회
     */
    public PostDto getPostDetail(long postId) {
        //postId에 해당하는 객체를 찾아옴
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new AppException(ErrorCode.POST_NOT_FOUND));

        return PostDto.of(post, post.getUser().getUserName());
    }

    /**
     * 게시글 전체 조회
     */
    public Page<PostDto> getAllPost(Pageable pageable) {
        Page<Post> posts = postRepository.findAll(pageable);
        Page<PostDto> postDtoList = posts.map(m -> PostDto.of(m, m.getUser().getUserName()));

        return postDtoList;
    }

    /**
     * 게시글 단건 삭제
     */
    public PostDto deletePost(long postId, String userName) {
        //글이 존재하지 않을때 지정 에러를 발생
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new AppException(ErrorCode.POST_NOT_FOUND));

        //유저이름이 존재하지 않을때 지정 에러를 발생
        User user = userRepository.findByUserName(userName)
                .orElseThrow(() -> new AppException(ErrorCode.USERNAME_NOT_FOUND));

        //작성자와 유저이름이 다를때 지정 에러를 발생
        if (!post.getUser().getUserName().equals(user.getUserName())) {
            throw new AppException(ErrorCode.INVALID_PERMISSION);
        }

        postRepository.delete(post);

        return PostDto.of(post, user.getUserName());

    }

    /**
     * 게시글 수정
     */
    public PostDto modifyPost(long postId, PostModifyRequest request, String userName) {
        //글이 존재하지 않을때 지정 에러를 발생
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new AppException(ErrorCode.POST_NOT_FOUND));

        //유저이름이 존재하지 않을때 지정 에러를 발생
        User user = userRepository.findByUserName(userName)
                .orElseThrow(() -> new AppException(ErrorCode.USERNAME_NOT_FOUND));

        //작성자와 유저이름이 다를때 지정 에러를 발생
        if (!post.getUser().getUserName().equals(userName)) {
            throw new AppException(ErrorCode.INVALID_PERMISSION);
        }

        String newTitle = request.getTitle();
        String newBody = request.getBody();

        post.modifyPost(newTitle, newBody);

        Post savedPost = postRepository.saveAndFlush(post);

        return PostDto.of(savedPost, user.getUserName());
    }

// --------------------------------------------------댓글--------------------------------------------------------------

    /**
     * 댓글 작성
     */
    public CommentDto writeComment(Long postId, CommentCreateRequest request, String jwtName) {
        //postId에 해당하는 게시글이 있는지 확인
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new AppException(ErrorCode.POST_NOT_FOUND));

        //JWT Token에 들어있는 userName이 userRepository에 있는지 확인
        User user = userRepository.findByUserName(jwtName)
                .orElseThrow(() -> new AppException(ErrorCode.USERNAME_NOT_FOUND));


        Comment comment = Comment.builder()
                .comment(request.getComment())
                .user(user)
                .post(post)
                .build();

        Comment savedComment = commentRepository.save(comment);

        //포스트작성자와 댓글작성자가 다르면 알람이 저장됨
        if (!post.getUser().equals(user)) {
            alarmRepository.save(Alarm.of(post.getUser(), user.getId(), postId, AlarmType.NEW_COMMENT_ON_POST));
        }

        return CommentDto.of(savedComment);
    }

    /**
     * 댓글 전체 조회
     */
    public Page<CommentListResponse> getAllComment(PageRequest pageable, long postId) {
        //postId에 해당하는 게시글이 있는지 확인
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new AppException(ErrorCode.POST_NOT_FOUND));

        return commentRepository.findCommentByPost(post, pageable).map(comment -> CommentListResponse.of(comment));
    }

    /**
     * 댓글 수정
     */
    public CommentDto modifyComment(long postId, long commentId, CommentModifyRequest request, String userName) {
        //게시글이 존재하지 않을 때 지정 에러를 발생
        postRepository.findById(postId)
                .orElseThrow(() -> new AppException(ErrorCode.POST_NOT_FOUND));

        //게시글에 해당 댓글이 존재하지 않을 때 지정 에러를 발생
        Comment comment = commentRepository.findById(commentId)
                .orElseThrow(() -> new AppException(ErrorCode.COMMENT_NOT_FOUND));

        //유저이름이 존재하지 않을때 지정 에러를 발생
        userRepository.findByUserName(userName)
                .orElseThrow(() -> new AppException(ErrorCode.USERNAME_NOT_FOUND));

        //코멘트 작성자와 유저 이름이 다를때 지정 에러를 발생
        if (!comment.getUser().getUserName().equals(userName)) {
            throw new AppException(ErrorCode.INVALID_PERMISSION);
        }

        //코멘트를 수정하는 과정
        String newComment = request.getComment();
        comment.modifyComment(newComment);
        Comment savedComment = commentRepository.saveAndFlush(comment);

        return CommentDto.of(savedComment);
    }

    /**
     * 댓글 삭제
     */
    public CommentDto deleteComment(long postId, long commentId, String userName) {
        //유저이름이 존재하지 않을때 지정 에러를 발생
        userRepository.findByUserName(userName)
                .orElseThrow(() -> new AppException(ErrorCode.USERNAME_NOT_FOUND));

        //게시글이 존재하지 않을 때 지정 에러를 발생
        postRepository.findById(postId)
                .orElseThrow(() -> new AppException(ErrorCode.POST_NOT_FOUND));

        //게시글에 해당 댓글이 존재하지 않을 때 지정 에러를 발생
        Comment comment = commentRepository.findById(commentId)
                .orElseThrow(() -> new AppException(ErrorCode.COMMENT_NOT_FOUND));

        //코멘트 작성자와 유저 이름이 다를때 지정 에러를 발생
        if (!comment.getUser().getUserName().equals(userName)) {
            throw new AppException(ErrorCode.INVALID_PERMISSION);
        }

        commentRepository.delete(comment);

        return CommentDto.of(comment);
    }

// --------------------------------------------------마이피드--------------------------------------------------------------

    /**
     * 마이 피드 조회
     */
    public Page<FeedResponse> getMyPost(String userName, Pageable pageable) {
        //유저이름이 존재하지 않을때 지정 에러를 발생
        User user = userRepository.findByUserName(userName)
                .orElseThrow(() -> new AppException(ErrorCode.USERNAME_NOT_FOUND));

        return postRepository.findPostByUser(user, pageable).map(post -> FeedResponse.of(post));
    }

// --------------------------------------------------좋아요--------------------------------------------------------------

    /**
     * 좋아요 기능
     */
    public LikeDto likePost(Long postId, String userName) {
        //유저이름이 존재하지 않을때 지정 에러를 발생
        User user = userRepository.findByUserName(userName)
                .orElseThrow(() -> new AppException(ErrorCode.USERNAME_NOT_FOUND));

        //게시글이 존재하지 않을 때 지정 에러를 발생
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new AppException(ErrorCode.POST_NOT_FOUND));

        Like newLike = Like.of(user, post);
        Like like = likeRepository.findByUserAndPost(user, post).orElseGet(() -> newLike);

        like.setActivation();
        Like savedLike = likeRepository.save(like);

        if (savedLike.isActivation()) {
            alarmRepository.save(Alarm.of(post.getUser(), user.getId(), postId, AlarmType.NEW_LIKE_ON_POST));
        }

        return LikeDto.of(savedLike);
    }

    /**
     * 좋아요 갯수 카운트
     */
    public LikeDto cntPostLikes(Long postId) {
        //게시글이 존재하지 않을 때 지정 에러를 발생
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new AppException(ErrorCode.POST_NOT_FOUND));

        //해당게시글에 있는 좋아요 중 Activation이 true인 경우만 카운트
        Long cnt = likeRepository.countLikeByPostAndActivationIsTrue(post);

        return LikeDto.cntOf(cnt);
    }
}
