package com.likelion.semifinal.service;

import com.likelion.semifinal.domain.dto.alarm.AlarmResponse;
import com.likelion.semifinal.domain.entity.User;
import com.likelion.semifinal.exception.AppException;
import com.likelion.semifinal.exception.ErrorCode;
import com.likelion.semifinal.repository.AlarmRepository;
import com.likelion.semifinal.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class AlarmService {
    private final UserRepository userRepository;
    private final AlarmRepository alarmRepository;
    /**
     * 알람 전체 조회
     */
    public Page<AlarmResponse> getMyAlarm(String userName, Pageable pageable) {
        //유저이름이 존재하지 않을때 지정 에러를 발생
        User user = userRepository.findByUserName(userName)
                .orElseThrow(() -> new AppException(ErrorCode.USERNAME_NOT_FOUND));

        return alarmRepository.findAlarmByUser(user, pageable).map(alarm -> AlarmResponse.of(alarm));
    }
}
