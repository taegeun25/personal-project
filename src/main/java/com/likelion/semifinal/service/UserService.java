package com.likelion.semifinal.service;

import com.likelion.semifinal.domain.dto.user.UserDto;
import com.likelion.semifinal.domain.dto.user.join.UserJoinRequest;
import com.likelion.semifinal.domain.dto.user.login.UserLoginRequest;
import com.likelion.semifinal.domain.entity.User;
import com.likelion.semifinal.exception.AppException;
import com.likelion.semifinal.exception.ErrorCode;
import com.likelion.semifinal.repository.UserRepository;
import com.likelion.semifinal.util.JwtUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder encoder;
    @Value("${jwt.token.secret}")
    private String key;
    private long expireTimeMs = 1000*60*60; //1시간

    /**
     * 로그인
     */
    public String login(UserLoginRequest request) {
        //userName 존재하지 않을경우 지정에러 발생
        User user = userRepository.findByUserName(request.getUserName())
                .orElseThrow(()-> new AppException(ErrorCode.USERNAME_NOT_FOUND));

        //password 일치하지 않을경우 지정에러 발생
        if(!encoder.matches(request.getPassword(), user.getPassword())) {
            throw new AppException(ErrorCode.INVALID_PASSWORD);
        }

        return JwtUtil.createToken(request.getUserName(), key, expireTimeMs);
    }

    /**
     * 회원가입
     */
    public UserDto join(UserJoinRequest request) {

        //userName 중복일경우 에러처리
        userRepository.findByUserName(request.getUserName())
                .ifPresent(user -> {
                    throw new AppException(ErrorCode.DUPLICATED_USER_NAME);
                });

        User savedUser = userRepository.save(request.toEntity(encoder.encode(request.getPassword())));

        return UserDto.of(savedUser);
    }
}
