package com.likelion.semifinal.repository;

import com.likelion.semifinal.domain.entity.Alarm;
import com.likelion.semifinal.domain.entity.Post;
import com.likelion.semifinal.domain.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;


public interface AlarmRepository extends JpaRepository<Alarm, Long> {

    Page<Alarm> findAlarmByUser(User user, Pageable pageable);
}
