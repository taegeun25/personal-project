package com.likelion.semifinal.repository;

import com.likelion.semifinal.domain.entity.Comment;
import com.likelion.semifinal.domain.entity.Post;
import com.likelion.semifinal.domain.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;



public interface PostRepository extends JpaRepository<Post, Long> {


    Page<Post> findPostByUser(User user, Pageable pageable);
}
