package com.likelion.semifinal.repository;

import com.likelion.semifinal.domain.entity.Like;
import com.likelion.semifinal.domain.entity.Post;
import com.likelion.semifinal.domain.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface LikeRepository extends JpaRepository<Like, Long> {
    Optional<Like> findByUserAndPost(User user, Post post);

    Long countLikeByPostAndActivationIsTrue(Post post);
}
