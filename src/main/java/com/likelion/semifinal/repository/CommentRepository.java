package com.likelion.semifinal.repository;

import com.likelion.semifinal.domain.entity.Comment;
import com.likelion.semifinal.domain.entity.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CommentRepository extends JpaRepository<Comment, Long> {

    Page<Comment> findCommentByPost(Post post, PageRequest pageable);
}
