package com.likelion.semifinal.config;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;

import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springdoc.core.GroupedOpenApi;
import org.springdoc.core.customizers.OpenApiCustomiser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@OpenAPIDefinition(
        info = @Info(
                title = "태근의 멋사 SNS",
                description = "게시글 CRUD, 좋아요, 알림설정 기능",
                version = "v1",
                contact = @Contact(name = "TaeGeun", email = "taegeun0525@gamil.com")
        ),
        tags = {
                @Tag(name = "1. 회원", description = "회원 가입/로그인"),
                @Tag(name = "2. 게시글", description = "게시글 CRUD"),
                @Tag(name = "3. 댓글", description = "댓글 CRUD"),
                @Tag(name = "4. 좋아요", description = "댓글,게시글 좋아요 기능"),
                @Tag(name = "5. 마이피드", description = "내 게시글 조회"),
                @Tag(name = "6. 알람", description = "내 게시글에 달린 댓글/좋아요 알림")
        }

)
@Configuration
public class SwaggerConfig {

        @Bean
        public GroupedOpenApi SecurityGroupOpenApi() {
                String[] paths = {"/api/v1/**"};

                return GroupedOpenApi
                        .builder()
                        .group("SNS 서비스 API v1")
                        .pathsToMatch(paths)
                        .addOpenApiCustomiser(buildSecurityOpenApi())
                        .build();
        }
//        @Bean
//        public GroupedOpenApi NonSecurityGroupOpenApi() {
//                String[] paths = {"/api/v1/**"};
//
//                return GroupedOpenApi
//                        .builder()
//                        .group("SNS 서비스 API Test")
//                        .pathsToMatch(paths)
//                        .build();
//        }

        public OpenApiCustomiser buildSecurityOpenApi() {
                // jwt token 을 한번 설정하면 header 에 값을 넣어주는 코드
                return OpenApi -> OpenApi.addSecurityItem(new SecurityRequirement().addList("jwt token"))
                        .getComponents().addSecuritySchemes("jwt token", new SecurityScheme()
                                .name("Authorization")
                                .type(SecurityScheme.Type.APIKEY) //받아올때 어느타입으로 받아올것인지
                                .in(SecurityScheme.In.HEADER) //jwt값이 Header에 담기므로 HEADER로 지정한다.
                                .bearerFormat("JWT")
                                .scheme("Bearer"));
        }


}
