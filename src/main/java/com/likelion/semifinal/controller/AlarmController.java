package com.likelion.semifinal.controller;


import com.likelion.semifinal.domain.dto.Response;
import com.likelion.semifinal.domain.dto.alarm.AlarmResponse;
import com.likelion.semifinal.service.AlarmService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1")
@Tag(name = "6. 알람")
public class AlarmController {

    private final  AlarmService alarmService;
    /**
     * 알람 (alarm)
     */
    @Operation(summary = "알람", description = "사용권한 : 유저")
    @GetMapping("/alarms")
    public Response<Page<AlarmResponse>> alarmList(@Parameter(hidden = true) Authentication authentication) {
        PageRequest pageable = PageRequest.of(0,20, Sort.by("alarmId"));
        String userName = authentication.getName();
        Page<AlarmResponse> alarms = alarmService.getMyAlarm(userName, pageable);
        return Response.success(alarms);
    }

}
