package com.likelion.semifinal.controller;

import com.likelion.semifinal.domain.dto.Response;
import com.likelion.semifinal.domain.dto.user.UserDto;
import com.likelion.semifinal.domain.dto.user.join.UserJoinRequest;
import com.likelion.semifinal.domain.dto.user.join.UserJoinResponse;
import com.likelion.semifinal.domain.dto.user.login.UserLoginRequest;
import com.likelion.semifinal.domain.dto.user.login.UserLoginResponse;
import com.likelion.semifinal.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/api/v1/users")
@RequiredArgsConstructor
@RestController
@Tag(name = "1. 회원")
public class UserController {
    private final UserService userService;

    /**
     * 회원가입 (join)
     */
    @Operation(summary = "회원가입", description = "사용권한 : 아무나")
    @PostMapping("/join")
    public Response<UserJoinResponse> join(@RequestBody UserJoinRequest request) {
        UserDto userDto = userService.join(request);
        return Response.success(new UserJoinResponse(userDto.getId(), userDto.getUserName()));
    }

    /**
     * 로그인 (login)
     */
    @Operation(summary = "로그인", description = "사용권한 : 회원")
    @PostMapping("/login")
    public Response<UserLoginResponse> login(@RequestBody UserLoginRequest userLoginRequest){
        String token = userService.login(userLoginRequest);
        return Response.success(new UserLoginResponse(token));
    }


}
