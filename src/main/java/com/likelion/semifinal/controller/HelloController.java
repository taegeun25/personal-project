package com.likelion.semifinal.controller;

import com.likelion.semifinal.service.HelloService;
import io.swagger.v3.oas.annotations.Hidden;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/hello")
@Hidden
public class HelloController {

    private final HelloService helloService;
    @GetMapping
    public ResponseEntity<String> hello() {
        return ResponseEntity.ok().body("박태근");
    }

    @GetMapping("/{num}")
    public ResponseEntity<Integer> helloSum(@PathVariable Integer num) {
        Integer sumResult = helloService.sumOfDigits(num);
        return ResponseEntity.ok().body(sumResult);
    }
}
