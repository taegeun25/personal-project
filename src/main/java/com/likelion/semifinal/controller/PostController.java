package com.likelion.semifinal.controller;


import com.likelion.semifinal.domain.dto.Response;
import com.likelion.semifinal.domain.dto.comment.*;
import com.likelion.semifinal.domain.dto.post.LikeDto;
import com.likelion.semifinal.domain.dto.post.PostDto;
import com.likelion.semifinal.domain.dto.post.like.LikeResponse;
import com.likelion.semifinal.domain.dto.post.delete.PostDeleteResponse;
import com.likelion.semifinal.domain.dto.post.detail.PostFindByIdResponse;
import com.likelion.semifinal.domain.dto.post.modify.PostModifyRequest;
import com.likelion.semifinal.domain.dto.post.modify.PostModifyResponse;
import com.likelion.semifinal.domain.dto.post.write.PostCreateRequest;
import com.likelion.semifinal.domain.dto.post.write.PostCreateResponse;
import com.likelion.semifinal.domain.dto.post.myfeed.FeedResponse;
import com.likelion.semifinal.service.PostService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;


@RequestMapping("/api/v1/posts")
@RequiredArgsConstructor
@RestController
public class PostController {

    private final PostService postService;


// --------------------------------------------------게시글--------------------------------------------------------------

    /**
     * 게시글 작성(write)
     */
    @Operation(summary = "게시글 작성", description = "사용권한 : 회원")
    @PostMapping
    @Tag(name = "2. 게시글")
    public Response<PostCreateResponse> postWrite(@RequestBody PostCreateRequest request, @Parameter(hidden = true) Authentication authentication) {
        String userName = authentication.getName();
        PostDto postDto = postService.writePost(request, userName);
        return Response.success(new PostCreateResponse("포스트 등록 완료", postDto.getPostId()));
    }

    /**
     * 게시글 단건 조회 (getDetail)
     */
    @Tag(name = "2. 게시글")
    @Operation(summary = "게시글 단건 조회", description = "사용권한 : 아무나")
    @GetMapping("/{postId}")
    public Response<PostFindByIdResponse> postGet(@PathVariable Long postId) {
        PostDto postDto = postService.getPostDetail(postId);
        PostFindByIdResponse postfindByIdResponse = PostFindByIdResponse.of(postDto);
        return Response.success(postfindByIdResponse);
    }

    /**
     * 게시글 전체 조회 (getAllPost)
     */
    @Tag(name = "2. 게시글")
    @Operation(summary = "게시글 전체 조회", description = "사용권한 : 아무나")
    @GetMapping("")
    public Response<Page<PostDto>> postList() {
        PageRequest pageable = PageRequest.of(0, 20, Sort.by("postId").descending());
        Page<PostDto> postDtos = postService.getAllPost(pageable);
        return Response.success(postDtos);
    }

    /**
     * 게시글 단건 삭제 (deletePost)
     */
    @Tag(name = "2. 게시글")
    @Operation(summary = "게시글 단건 삭제", description = "사용권한 : 작성자")
    @DeleteMapping("/{postId}")
    public Response<PostDeleteResponse> postDelete(@PathVariable long postId, @Parameter(hidden = true) Authentication authentication) {
        String userName = authentication.getName();
        PostDto postDto = postService.deletePost(postId, userName);
        return Response.success(new PostDeleteResponse("포스트 삭제 완료", postDto.getPostId()));
    }

    /**
     * 게시글 수정 (modify)
     */
    @Tag(name = "2. 게시글")
    @Operation(summary = "게시글 수정", description = "사용권한 : 작성자")
    @PutMapping("/{postId}")
    public Response<PostModifyResponse> postModify(@PathVariable long postId, @RequestBody PostModifyRequest request, @Parameter(hidden = true) Authentication authentication) {
        String userName = authentication.getName();
        PostDto postDto = postService.modifyPost(postId, request, userName);
        return Response.success(new PostModifyResponse("포스트 수정 완료", postDto.getPostId()));
    }

// --------------------------------------------------댓글--------------------------------------------------------------

    /**
     * 댓글 작성(write)
     */
    @Tag(name = "3. 댓글")
    @Operation(summary = "댓글 작성", description = "사용권한 : 유저")
    @PostMapping("/{postsId}/comments")
    public Response<CommentCreateResponse> commentWrite(@PathVariable Long postsId, @RequestBody CommentCreateRequest request, @Parameter(hidden = true) Authentication authentication) {
        String userName = authentication.getName();
        CommentDto commentDto = postService.writeComment(postsId, request, userName);
        return Response.success(CommentCreateResponse.of(commentDto));
    }

    /**
     * 댓글 조회(getAllComment)
     */
    @Tag(name = "3. 댓글")
    @Operation(summary = "댓글 조회", description = "사용권한 : 아무나")
    @GetMapping("/{postId}/comments")
    public Response<Page<CommentListResponse>> commentList(@PathVariable long postId) {
        PageRequest pageable = PageRequest.of(0, 10, Sort.by("commentId").descending());
        Page<CommentListResponse> commentDtos = postService.getAllComment(pageable, postId);
        return Response.success(commentDtos);
    }

    /**
     * 댓글 수정(commentModify
     */
    @Tag(name = "3. 댓글")
    @Operation(summary = "댓글 수정", description = "사용권한 : 작성자")
    @PutMapping("/{postId}/comments/{commentId}")
    public Response<CommentModifyResponse> commentModify(@PathVariable long postId, @PathVariable long commentId, @RequestBody CommentModifyRequest request, @Parameter(hidden = true) Authentication authentication) {
        String userName = authentication.getName();
        CommentDto commentDto = postService.modifyComment(postId, commentId, request, userName);
        return Response.success(CommentModifyResponse.of(commentDto));
    }

    /**
     * 댓글 단건 삭제
     */
    @Tag(name = "3. 댓글")
    @Operation(summary = "댓글 삭제", description = "사용권한 : 작성자")
    @DeleteMapping("/{postId}/comments/{commentId}")
    public Response<CommentDeleteResponse> commentDelete(@PathVariable long postId, @PathVariable long commentId, @Parameter(hidden = true) Authentication authentication) {
        String userName = authentication.getName();
        CommentDto commentDto = postService.deleteComment(postId, commentId, userName);
        return Response.success(CommentDeleteResponse.of(commentDto));
    }

// --------------------------------------------------마이피드--------------------------------------------------------------

    /**
     * 마이피드 조회
     */
    @Tag(name = "5. 마이피드")
    @Operation(summary = "마이피드", description = "사용권한 : 유저")
    @GetMapping("/my")
    public Response<Page<FeedResponse>> feedList(@Parameter(hidden = true) Authentication authentication) {
        PageRequest pageable = PageRequest.of(0, 20, Sort.by("postId").descending());
        String userName = authentication.getName();
        Page<FeedResponse> posts = postService.getMyPost(userName,pageable);
        return Response.success(posts);
    }

// --------------------------------------------------좋아요--------------------------------------------------------------

    /**
     * 좋아요 기능
     */
    @Tag(name = "4. 좋아요")
    @Operation(summary = "좋아요", description = "사용권한 : 유저")
    @PostMapping("/{postId}/likes")
    public Response<LikeResponse> postLike(@PathVariable Long postId, @Parameter(hidden = true) Authentication authentication) {
        String userName = authentication.getName();
        LikeDto likeDto = postService.likePost(postId,userName);
        return Response.success(LikeResponse.of(likeDto.isActivation()));
    }

    /**
     * 좋아요 갯수 카운트
     */
    @Tag(name = "4. 좋아요")
    @Operation(summary = "좋아요 갯수", description = "사용권한 : 아무나")
    @GetMapping("/{postId}/likes")
    public Response<Long> postLikesCnt(@PathVariable Long postId) {
        LikeDto likeDto = postService.cntPostLikes(postId);
        return Response.success(likeDto.getLikeCnt());
    }
}
