package com.likelion.semifinal.domain.dto.comment;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Builder
public class CommentModifyResponse {

    private Long id;
    private String comment;
    private String userName;
    private Long postId;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss",timezone = "Asia/Seoul")
    private LocalDateTime createdAt;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss",timezone = "Asia/Seoul")
    private LocalDateTime lastModifiedAt;

    public static CommentModifyResponse of(CommentDto commentDto) {
        return CommentModifyResponse.builder()
                .id(commentDto.getCommentId())
                .comment(commentDto.getComment())
                .userName(commentDto.getUser().getUserName())
                .postId(commentDto.getPost().getPostId())
                .createdAt(commentDto.getCreatedAt())
                .lastModifiedAt(commentDto.getLastModifiedAt())
                .build();
    }
}
