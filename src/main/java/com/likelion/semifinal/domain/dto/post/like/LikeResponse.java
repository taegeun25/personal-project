package com.likelion.semifinal.domain.dto.post.like;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class LikeResponse {
    private String result;

    public static LikeResponse of(boolean likes) {
        LikeResponse likeResponse = new LikeResponse();

        if (likes) {
            likeResponse.result = "좋아요를 눌렀습니다";
        } else likeResponse.result = "좋아요를 취소했습니다";

        return likeResponse;
    }
}
