package com.likelion.semifinal.domain.dto;

import com.likelion.semifinal.exception.ErrorCode;
import lombok.Getter;

@Getter
public class ErrorResponse {
    private ErrorCode errorCode;
    private String message;

    public ErrorResponse(ErrorCode errorCode, String message) {
        this.errorCode = errorCode;
        this.message = errorCode.getMessage();
    }
}
