package com.likelion.semifinal.domain.dto.post;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.likelion.semifinal.domain.entity.Like;
import com.likelion.semifinal.domain.entity.Post;
import com.likelion.semifinal.domain.entity.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class LikeDto {
    private Long likeId;
    private boolean activation;
    private Post post;
    private User user;
    private Long likeCnt;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Seoul")
    private LocalDateTime createdAt;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Seoul")
    private LocalDateTime lastModifiedAt;

    public static LikeDto of(Like like) {
        return LikeDto.builder()
                .likeId(like.getLikeId())
                .user(like.getUser())
                .post(like.getPost())
                .activation(like.isActivation())
                .createdAt(like.getCreatedAt())
                .lastModifiedAt(like.getLastModifiedAt())
                .build();
    }

    public static LikeDto cntOf(Long likeCnt) {
        return LikeDto.builder()
                .likeCnt(likeCnt)
                .build();
    }
}
