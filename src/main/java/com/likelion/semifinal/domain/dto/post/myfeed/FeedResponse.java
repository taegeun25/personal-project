package com.likelion.semifinal.domain.dto.post.myfeed;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.likelion.semifinal.domain.entity.Post;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FeedResponse {
    private long id;
    private String title;
    private String userName;
    private String body;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Seoul")
    private LocalDateTime createdAt;

    public static FeedResponse of(Post post) {
        return FeedResponse.builder()
                .id(post.getPostId())
                .title(post.getTitle())
                .userName(post.getUser().getUserName())
                .body(post.getBody())
                .createdAt(post.getCreatedAt())
                .build();
    }
}
