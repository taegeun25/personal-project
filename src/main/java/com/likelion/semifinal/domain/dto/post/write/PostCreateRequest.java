package com.likelion.semifinal.domain.dto.post.write;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class PostCreateRequest {
    private String title;
    private String body;

}
