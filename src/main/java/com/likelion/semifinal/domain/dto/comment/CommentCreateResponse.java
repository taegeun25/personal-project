package com.likelion.semifinal.domain.dto.comment;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class CommentCreateResponse {
    private Long id;
    private String comment;
    private String userName;
    private Long postId;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss",timezone = "Asia/Seoul")
    private LocalDateTime createdAt;

    public static CommentCreateResponse of(CommentDto commentDto) {
        CommentCreateResponse commentCreateResponse = new CommentCreateResponse();
        commentCreateResponse.id = commentDto.getCommentId();
        commentCreateResponse.comment = commentDto.getComment();
        commentCreateResponse.userName = commentDto.getUser().getUserName();
        commentCreateResponse.postId = commentDto.getPost().getPostId();
        commentCreateResponse.createdAt = commentDto.getCreatedAt();
        return commentCreateResponse;
    }
}
