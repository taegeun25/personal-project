package com.likelion.semifinal.domain.dto.post.like;

import com.likelion.semifinal.domain.dto.post.LikeDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class LikeCntResponse {
     private Long result;

     public static LikeCntResponse of(LikeDto likeDto) {
          return new LikeCntResponse(likeDto.getLikeCnt());
     }
}
