package com.likelion.semifinal.domain.dto.user.login;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Getter
public class UserLoginResponse {
    private String jwt;
}
