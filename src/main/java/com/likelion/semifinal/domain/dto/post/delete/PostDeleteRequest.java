package com.likelion.semifinal.domain.dto.post.delete;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class PostDeleteRequest {
    private long id;
}
