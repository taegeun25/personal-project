package com.likelion.semifinal.domain.dto.post;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.likelion.semifinal.domain.entity.Post;
import lombok.*;
import java.time.LocalDateTime;

@Getter
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class PostDto {
    private Long postId;
    private String title;
    private String body;
    private String userName;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Seoul")
    private LocalDateTime createdAt;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Seoul")
    private LocalDateTime lastModifiedAt;

    public static PostDto of(Post post, String userName) {
        return PostDto.builder()
                .postId(post.getPostId())
                .title(post.getTitle())
                .body(post.getBody())
                .userName(userName)
                .createdAt(post.getCreatedAt())
                .lastModifiedAt(post.getLastModifiedAt())
                .build();
    }

}
