package com.likelion.semifinal.domain.dto.post.modify;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class PostModifyRequest {
    private String title;
    private String body;

}
