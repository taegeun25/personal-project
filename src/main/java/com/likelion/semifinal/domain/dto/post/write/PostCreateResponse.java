package com.likelion.semifinal.domain.dto.post.write;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class PostCreateResponse {
    private String body;
    private Long postId;
}
