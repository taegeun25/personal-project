package com.likelion.semifinal.domain.dto.comment;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class CommentDeleteResponse {
    private String message;
    private long id;


    public static CommentDeleteResponse of(CommentDto commentDto) {
        return CommentDeleteResponse.builder()
                .message("댓글 삭제 완료")
                .id(commentDto.getCommentId())
                .build();
    }

}
