package com.likelion.semifinal.domain.dto.post.delete;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class PostDeleteResponse {
    private String message;
    private long postId;
}
