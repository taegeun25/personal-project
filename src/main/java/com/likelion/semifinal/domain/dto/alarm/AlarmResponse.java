package com.likelion.semifinal.domain.dto.alarm;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.likelion.semifinal.domain.entity.Alarm;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Builder
public class AlarmResponse {
    private long id;
    private AlarmType alarmType;
    private long fromUserId;
    private long targetId;
    private String text;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Seoul")
    private LocalDateTime createAt;

    public static AlarmResponse of(Alarm alarm) {

        return AlarmResponse.builder()
                .id(alarm.getAlarmId())
                .alarmType(alarm.getAlarmType())
                .fromUserId(alarm.getFromUserId())
                .targetId(alarm.getTargetId())
                .text(alarm.getAlarmType().getText())
                .createAt(alarm.getCreatedAt())
                .build();

    }
}
