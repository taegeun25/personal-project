package com.likelion.semifinal.domain.dto.post.modify;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class PostModifyResponse {
    private String message;
    private long postId;
}
