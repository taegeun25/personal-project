package com.likelion.semifinal.domain.dto.comment;

import com.likelion.semifinal.domain.entity.Comment;
import com.likelion.semifinal.domain.entity.Post;
import com.likelion.semifinal.domain.entity.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class CommentDto {

    private long commentId;
    private String comment;
    private User user;
    private Post post;
    private LocalDateTime createdAt;
    private LocalDateTime lastModifiedAt;

    public static CommentDto of(Comment comment) {
        return CommentDto.builder()
                .commentId(comment.getCommentId())
                .comment(comment.getComment())
                .createdAt(comment.getCreatedAt())
                .lastModifiedAt(comment.getLastModifiedAt())
                .user(comment.getUser())
                .post(comment.getPost())
                .build();
    }


}
