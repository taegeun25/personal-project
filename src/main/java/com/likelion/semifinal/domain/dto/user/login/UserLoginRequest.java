package com.likelion.semifinal.domain.dto.user.login;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@AllArgsConstructor
public class UserLoginRequest {
    private String userName;
    private String password;
}
