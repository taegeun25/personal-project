package com.likelion.semifinal.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@AllArgsConstructor
@Getter
@NoArgsConstructor
@Builder
@Table(name = "likes")
public class Like extends BaseTimeEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long likeId;
    private boolean activation;

    @ManyToOne
    @JoinColumn(name = "User_Id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "Post_Id")
    private Post post;

    public void setActivation() {
        this.activation = !this.activation;
    }

    public static Like of(User user, Post post) {

        return Like.builder()
                .activation(false)
                .user(user)
                .post(post)
                .build();
    }
}
