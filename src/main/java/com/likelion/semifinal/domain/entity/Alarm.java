package com.likelion.semifinal.domain.entity;

import com.likelion.semifinal.domain.dto.alarm.AlarmType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder

public class Alarm extends BaseTimeEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long alarmId;

    @Enumerated(EnumType.STRING)
    private AlarmType alarmType;

    //알람을 발생시킨 user의 id
    private long fromUserId;

    //알람이 발생된 post의 id
    private long targetId;

    private LocalDateTime deletedAt;

    //알람 받는 사람
    @ManyToOne
    @JoinColumn(name = "User_Id")
    private User user;

    public static Alarm of(User user, long fromUserId, long targetId, AlarmType alarmType) {
        return Alarm.builder()
                .alarmType(alarmType)
                .fromUserId(fromUserId)
                .targetId(targetId)
                .user(user)
                .build();
    }
}
