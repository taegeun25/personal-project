package com.likelion.semifinal.fixture;

import com.likelion.semifinal.domain.entity.Comment;
import com.likelion.semifinal.domain.entity.Post;
import com.likelion.semifinal.domain.entity.User;

public class CommentFixture {
    public static Comment get(String userName, String password) {
        return Comment.builder()
                .commentId(1l)
                .comment("댓글")
                .post(PostFixture.get(userName,password))
                .user(UserFixture.get(userName,password))
                .build();
    }

}
