package com.likelion.semifinal.fixture;

import com.likelion.semifinal.domain.entity.Post;

public class PostFixture {
    public static Post get(String userName, String password) {
        return Post.builder()
                .postId(1l)
                .title("제목")
                .body("내용")
                .user(UserFixture.get(userName,password))
                .build();
    }

}
