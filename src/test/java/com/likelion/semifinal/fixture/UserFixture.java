package com.likelion.semifinal.fixture;

import com.likelion.semifinal.domain.entity.User;

public class UserFixture {
    public static User get(String userName, String password) {
        return User.builder()
                .userName(userName)
                .password(password)
                .build();
    }
}
