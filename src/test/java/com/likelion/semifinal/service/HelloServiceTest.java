package com.likelion.semifinal.service;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HelloServiceTest {

    HelloService helloService = new HelloService();

    @Test
    @DisplayName("자리수의 합을 잘 구하는지")
    void sumOfDigit() {
        assertEquals(21, helloService.sumOfDigits(687));
        assertEquals(22, helloService.sumOfDigits(787));
        assertEquals(0, helloService.sumOfDigits(0));
        assertEquals(5, helloService.sumOfDigits(11111));
    }

}