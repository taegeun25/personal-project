package com.likelion.semifinal.service;

import com.likelion.semifinal.domain.dto.comment.CommentDto;
import com.likelion.semifinal.domain.dto.comment.CommentModifyRequest;
import com.likelion.semifinal.domain.dto.post.write.PostCreateRequest;
import com.likelion.semifinal.domain.dto.post.PostDto;
import com.likelion.semifinal.domain.dto.post.modify.PostModifyRequest;
import com.likelion.semifinal.domain.entity.Comment;
import com.likelion.semifinal.domain.entity.Post;
import com.likelion.semifinal.domain.entity.User;
import com.likelion.semifinal.exception.AppException;
import com.likelion.semifinal.exception.ErrorCode;
import com.likelion.semifinal.fixture.CommentFixture;
import com.likelion.semifinal.fixture.UserFixture;
import com.likelion.semifinal.repository.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;

class PostServiceTest {
    private PostService postService;
    PostRepository postRepository = Mockito.mock(PostRepository.class);
    UserRepository userRepository = Mockito.mock(UserRepository.class);
    CommentRepository commentRepository = Mockito.mock(CommentRepository.class);
    LikeRepository likeRepository = Mockito.mock(LikeRepository.class);
    AlarmRepository alarmRepository = Mockito.mock(AlarmRepository.class);

    @BeforeEach
    void setUp() {
        postService = new PostService(postRepository, userRepository,commentRepository,likeRepository,alarmRepository);

    }

    @Test
    @DisplayName("등록 성공")
    void post_write_success() {
        //Jwt에 있던 유저이름
        String userNameJwt = "태근";
        PostCreateRequest request = new PostCreateRequest("제목", "내용");
        //저장되어있는 유저
        User user = new User(1l, "태근", "1234");

        Post post = Post.builder()
                .postId(1l)
                .title("제목")
                .body("내용")
                .user(user)
                .build();

        when(userRepository.findByUserName(userNameJwt))
                .thenReturn(Optional.of(user));

        when(postRepository.save(any()))
                .thenReturn(post);

        PostDto postDto = postService.writePost(request, userNameJwt);

        assertEquals("제목", postDto.getTitle());
        assertEquals("내용", postDto.getBody());
        assertEquals(userNameJwt, postDto.getUserName());

    }

    @Test
    @DisplayName("등록 실패: 유저가 존재하지 않을때")
    void post_write_fail() {

        //Jwt에 있던 유저이름
        String userNameJwt = "태근";
        PostCreateRequest request = new PostCreateRequest("제목", "내용");

        //저장되어있는 유저
        User user = new User(1l, "태근", "1234");

        Post post = Post.builder()
                .postId(1l)
                .title("제목")
                .body("내용")
                .user(user)
                .build();

        when(userRepository.findByUserName(userNameJwt))
                .thenReturn(Optional.empty());
        when(postRepository.save(any()))
                .thenReturn(post);

        //발생한 Error를 받아오는 방법
        AppException exception = Assertions.assertThrows(AppException.class, () -> postService.writePost(request, userNameJwt));

        assertEquals(ErrorCode.USERNAME_NOT_FOUND, exception.getErrorCode());
        assertEquals(userNameJwt + "이 없습니다", exception.getMessage());
    }

    @Test
    @DisplayName("조회 성공")
    void post_get_success() {
        long postId = 1l;
        User user = User.builder()
                .id(1l)
                .userName("태근")
                .build();
        Post post = Post.builder()
                .postId(1l)
                .title("제목")
                .body("내용")
                .user(user)
                .build();

        when(postRepository.findById(postId))
                .thenReturn(Optional.of(post));

        PostDto postDto = postService.getPostDetail(postId);

        assertEquals(1l, postDto.getPostId());
        assertEquals("제목", postDto.getTitle());
        assertEquals("내용", postDto.getBody());
        assertEquals("태근", postDto.getUserName());
    }

    @Test
    @DisplayName("수정 성공")
    void post_modify_success() {
        long postId = 1l;
        String userNameJwt = "태근";
        PostModifyRequest request = new PostModifyRequest("제목 변경됨", "내용 변경됨");

        User user = User.builder()
                .id(1l)
                .userName("태근")
                .build();

        Post unmodifiedPost = Post.builder()
                .postId(1l)
                .title("제목")
                .body("내용")
                .user(user)
                .build();

        Post savedPost = Post.builder()
                .postId(1l)
                .title(request.getTitle())
                .body(request.getBody())
                .build();

        when(postRepository.findById(postId))
                .thenReturn(Optional.of(unmodifiedPost));

        when(userRepository.findByUserName(userNameJwt))
                .thenReturn(Optional.of(user));

        when(postRepository.saveAndFlush(any()))
                .thenReturn(savedPost);

        PostDto postDto = postService.modifyPost(postId,request,userNameJwt);

        assertEquals(request.getTitle(),postDto.getTitle());
        assertEquals(request.getBody(),postDto.getBody());

    }
    @Test
    @DisplayName("수정 실패 - 포스트 존재하지 않음")
    void post_modify_fail1() {
        long postId = 1l;
        String userNameJwt = "태근";
        PostModifyRequest request = new PostModifyRequest("제목 변경됨", "내용 변경됨");


        when(postRepository.findById(postId))
                .thenReturn(Optional.empty());


        AppException exception = Assertions.assertThrows(AppException.class, () -> postService.modifyPost(postId, request, userNameJwt));

        assertEquals(ErrorCode.POST_NOT_FOUND, exception.getErrorCode());

    }

    @Test
    @DisplayName("수정 실패 - 유저 존재하지 않음")
    void post_modify_fail2() {
        long postId = 1l;
        String userNameJwt = "태근";
        PostModifyRequest request = new PostModifyRequest("제목 변경됨", "내용 변경됨");


        User user = User.builder()
                .id(1l)
                .userName("구찌")
                .build();

        Post unmodifiedPost = Post.builder()
                .postId(1l)
                .title("제목")
                .body("내용")
                .user(user)
                .build();

        when(postRepository.findById(postId))
                .thenReturn(Optional.of(unmodifiedPost));

        when(userRepository.findByUserName(userNameJwt))
                .thenReturn(Optional.empty());


        AppException exception = Assertions.assertThrows(AppException.class, () -> postService.modifyPost(postId, request, userNameJwt));

        assertEquals(ErrorCode.USERNAME_NOT_FOUND, exception.getErrorCode());

    }

    @Test
    @DisplayName("수정 실패 - 작성자 =! 유저")
    void post_modify_fail3() {
        long postId = 1l;
        String userNameJwt = "태근";
        PostModifyRequest request = new PostModifyRequest("제목 변경됨", "내용 변경됨");



        User user1 = User.builder()
                .id(1l)
                .userName("구찌")
                .build();

        User user2 = User.builder()
                .id(1l)
                .userName("태근")
                .build();

        Post unmodifiedPost = Post.builder()
                .postId(1l)
                .title("제목")
                .body("내용")
                .user(user1)
                .build();

        when(postRepository.findById(postId))
                .thenReturn(Optional.of(unmodifiedPost));

        when(userRepository.findByUserName(userNameJwt))
                .thenReturn(Optional.of(user2));

        AppException exception = Assertions.assertThrows(AppException.class, () -> postService.modifyPost(postId, request, userNameJwt));

        assertEquals(ErrorCode.INVALID_PERMISSION, exception.getErrorCode());

    }

    @Test
    @DisplayName("삭제 실패 - 포스트 존재하지 않음")
    void post_delete_fail1() {
        long postId = 1l;
        String userNameJwt = "태근";

        when(postRepository.findById(postId))
                .thenReturn(Optional.empty());


        AppException exception = Assertions.assertThrows(AppException.class, () -> postService.deletePost(postId, userNameJwt));

        assertEquals(ErrorCode.POST_NOT_FOUND, exception.getErrorCode());

    }

    @Test
    @DisplayName("삭제 실패 - 유저 존재하지 않음")
    void post_delete_fail2() {
        long postId = 1l;
        String userNameJwt = "태근";
        Post post = Post.builder()
                .postId(1l)
                .title("제목")
                .body("내용")
                .build();

        when(postRepository.findById(postId))
                .thenReturn(Optional.of(post));

        when(userRepository.findByUserName(userNameJwt))
                .thenReturn(Optional.empty());

        AppException exception = Assertions.assertThrows(AppException.class, () -> postService.deletePost(postId, userNameJwt));

        assertEquals(ErrorCode.USERNAME_NOT_FOUND, exception.getErrorCode());

    }

    @Test
    @DisplayName("삭제 실패 - 작성자와 유저가 존재하지 않음")
    void post_delete_fail3() {
        long postId = 1l;
        String userNameJwt = "태근";

        User user1 = User.builder()
                .id(1l)
                .userName("구찌")
                .build();

        User user2 = User.builder()
                .id(2l)
                .userName("태근")
                .build();

        Post post = Post.builder()
                .postId(1l)
                .title("제목")
                .body("내용")
                .user(user1)
                .build();

        when(postRepository.findById(postId))
                .thenReturn(Optional.of(post));

        when(userRepository.findByUserName(userNameJwt))
                .thenReturn(Optional.of(user2));

        AppException exception = Assertions.assertThrows(AppException.class, () -> postService.deletePost(postId, userNameJwt));

        assertEquals(ErrorCode.INVALID_PERMISSION, exception.getErrorCode());

    }

    @Test
    @DisplayName("댓글 수정 성공")
    void comment_modify_success() {
        CommentModifyRequest request = new CommentModifyRequest("변경된 댓글");
        Comment comment = CommentFixture.get("user1","password");
        Comment savedComment = comment;
        savedComment.modifyComment(request.getComment());

        given(userRepository.findByUserName(any()))
                .willReturn(Optional.of(comment.getUser()));

        given(postRepository.findById(any()))
                .willReturn(Optional.of(new Post()));

        given(commentRepository.findById(any()))
                .willReturn(Optional.of(comment));

        given(commentRepository.saveAndFlush(any()))
                .willReturn(savedComment);

        CommentDto commentDto = postService.modifyComment(1l,1l,request,"user1");

        assertEquals("변경된 댓글", commentDto.getComment());
    }

    @Test
    @DisplayName("댓글 수정 실패(1) 포스트 존재하지 않음")
    void comment_modify_fail1() {
        CommentModifyRequest request = new CommentModifyRequest("변경된 댓글");

        AppException exception = assertThrows(AppException.class,
                () ->postService.modifyComment(1l,1l,request,"댓글작성자"));

        assertEquals(ErrorCode.POST_NOT_FOUND, exception.getErrorCode());
    }

    @Test
    @DisplayName("댓글 수정 실패(2) 게시글에 해당 댓글이 존재하지 않음")
    void comment_modify_fail2() {
        CommentModifyRequest request = new CommentModifyRequest("변경된 댓글");

        given(postRepository.findById(any()))
                .willReturn(Optional.of(new Post()));


        AppException exception = assertThrows(AppException.class,
                () ->postService.modifyComment(1l,1l,request,"댓글작성자"));

        assertEquals(ErrorCode.COMMENT_NOT_FOUND, exception.getErrorCode());
    }

    @Test
    @DisplayName("댓글 수정 실패(3) 유저가 존재하지 않음")
    void comment_modify_fail3() {
        CommentModifyRequest request = new CommentModifyRequest("변경된 댓글");
        Comment comment = CommentFixture.get("작성자","password");

        given(postRepository.findById(any()))
                .willReturn(Optional.of(comment.getPost()));

        given(commentRepository.findById(any()))
                .willReturn(Optional.of(comment));

        AppException exception = assertThrows(AppException.class,
                () ->postService.modifyComment(1l,1l,request,"낯선작성자"));

        assertEquals(ErrorCode.USERNAME_NOT_FOUND, exception.getErrorCode());
    }
    @Test
    @DisplayName("댓글 수정 실패(4) 댓글작성자!=유저")
    void comment_modify_fail4() {
        CommentModifyRequest request = new CommentModifyRequest("변경된 댓글");
        Comment comment = CommentFixture.get("작성자","password");
        User user = UserFixture.get("낯선작성자","password");

        given(postRepository.findById(any()))
                .willReturn(Optional.of(comment.getPost()));

        given(commentRepository.findById(any()))
                .willReturn(Optional.of(comment));

        given(userRepository.findByUserName(any()))
                .willReturn(Optional.of(user));

        AppException exception = assertThrows(AppException.class,
                () ->postService.modifyComment(1l,1l,request,"낯선작성자"));

        assertEquals(ErrorCode.INVALID_PERMISSION, exception.getErrorCode());
    }


}