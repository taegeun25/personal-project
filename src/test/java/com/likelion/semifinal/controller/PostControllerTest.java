package com.likelion.semifinal.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.likelion.semifinal.domain.dto.comment.CommentCreateRequest;
import com.likelion.semifinal.domain.dto.comment.CommentDto;
import com.likelion.semifinal.domain.dto.comment.CommentListResponse;
import com.likelion.semifinal.domain.dto.comment.CommentModifyRequest;
import com.likelion.semifinal.domain.dto.post.LikeDto;
import com.likelion.semifinal.domain.dto.post.PostDto;
import com.likelion.semifinal.domain.dto.post.modify.PostModifyRequest;
import com.likelion.semifinal.domain.dto.post.myfeed.FeedResponse;
import com.likelion.semifinal.domain.dto.post.write.PostCreateRequest;
import com.likelion.semifinal.domain.entity.Post;
import com.likelion.semifinal.domain.entity.User;
import com.likelion.semifinal.exception.AppException;
import com.likelion.semifinal.exception.ErrorCode;
import com.likelion.semifinal.fixture.PostFixture;
import com.likelion.semifinal.fixture.UserFixture;
import com.likelion.semifinal.repository.PostRepository;
import com.likelion.semifinal.service.PostService;
import com.likelion.semifinal.service.UserService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.*;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(PostController.class)
class PostControllerTest {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    UserService userService;

    @MockBean
    PostService postService;

    @MockBean
    PostRepository postRepository;

    @Autowired
    ObjectMapper objectMapper;

    Pageable pageable = PageRequest.of(0, 20, Sort.Direction.DESC, "createdAt");

    @Test
    @DisplayName("게시글 등록 성공")
    @WithMockUser
    void post_write_success() throws Exception {

        PostCreateRequest postCreateRequest = new PostCreateRequest("제목", "내용");


        when(postService.writePost(any(), any()))
                .thenReturn(PostDto.builder()
                        .postId(1L)
                        .build());

        mockMvc.perform(post("/api/v1/posts")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(postCreateRequest)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result.postId").exists())
                .andExpect(jsonPath("$.result.body").exists());

    }

    @Test
    @DisplayName("게시글 등록 실패 -토큰인증실패")
    @WithAnonymousUser
        //인증안된상태
    void post_write_fail() throws Exception {

        PostCreateRequest postCreateRequest = new PostCreateRequest("제목", "내용");


        when(postService.writePost(any(), any()))
                .thenThrow(new AppException(ErrorCode.INVALID_TOKEN));

        mockMvc.perform(post("/api/v1/posts")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(postCreateRequest)))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }


    @Test
    @DisplayName("게시글 조회 성공")
    @WithMockUser
    void post_get_success() throws Exception {
        PostDto post = PostDto.builder()
                .postId(1l)
                .title("제목")
                .body("내용")
                .userName("태근")
                .createdAt(LocalDateTime.now())
                .lastModifiedAt(LocalDateTime.now())
                .build();

        when(postService.getPostDetail(anyLong()))
                .thenReturn(post);

        mockMvc.perform(get("/api/v1/posts/1")
                        .with(csrf()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result.id").exists())
                .andExpect(jsonPath("$.result.title").exists())
                .andExpect(jsonPath("$.result.body").exists())
                .andExpect(jsonPath("$.result.userName").exists())
                .andExpect(jsonPath("$.result.createdAt").exists());
    }

    @Test
    @DisplayName("게시글 수정 실패(1) 인증실패")
    @WithAnonymousUser
    void post_modify_fail1() throws Exception {
        PostModifyRequest request = new PostModifyRequest("변경제목", "변경내용");

        when(postService.modifyPost(anyLong(), any(), any()))
                .thenThrow(new AppException(ErrorCode.INVALID_PERMISSION));

        mockMvc.perform(put("/api/v1/posts/1")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(request)))
                .andDo(print())
                .andExpect(status().isUnauthorized());

    }

    @Test
    @DisplayName("게시글 수정 실패(2) 작성자 불일치")
    @WithMockUser
    void post_modify_fail2() throws Exception {
        PostModifyRequest request = new PostModifyRequest("변경제목", "변경내용");

        when(postService.modifyPost(anyLong(), any(), any()))
                .thenThrow(new AppException(ErrorCode.POST_NOT_FOUND));

        mockMvc.perform(put("/api/v1/posts/1")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(request)))
                .andDo(print())
                .andExpect(status().is(ErrorCode.POST_NOT_FOUND.getHttpStatus().value()));

    }

    @Test
    @DisplayName("게시글 수정 실패(3) 데이터베이스 에러")
    @WithMockUser
    void post_modify_fail3() throws Exception {
        PostModifyRequest request = new PostModifyRequest("변경제목", "변경내용");

        when(postService.modifyPost(anyLong(), any(), any()))
                .thenThrow(new AppException(ErrorCode.DATABASE_ERROR));

        mockMvc.perform(put("/api/v1/posts/1")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(request)))
                .andDo(print())
                .andExpect(status().is(ErrorCode.DATABASE_ERROR.getHttpStatus().value()));
    }

    @Test
    @DisplayName("게시글 수정 성공")
    @WithMockUser
    void post_modify_success() throws Exception {
        PostModifyRequest request = new PostModifyRequest("변경제목", "변경내용");

        PostDto postDto = PostDto.builder()
                .postId(1l)
                .build();

        when(postService.modifyPost(anyLong(), any(), any()))
                .thenReturn(postDto);

        mockMvc.perform(put("/api/v1/posts/1")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(request)))
                .andDo(print())
                .andExpect(jsonPath("$.result.postId").exists())
                .andExpect(jsonPath("$.result.message").exists());
    }


    @Test
    @DisplayName("게시글 삭제 성공")
    @WithMockUser
    void post_delete_success() throws Exception {
        PostDto post = PostDto.builder()
                .postId(1l)
                .title("제목")
                .body("내용")
                .userName("태근")
                .createdAt(LocalDateTime.now())
                .build();

        when(postService.getPostDetail(1l))
                .thenReturn(post);

        mockMvc.perform(get("/api/v1/posts/1")
                        .with(csrf()))
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("게시글 삭제 실패(1) 인증실패")
    @WithAnonymousUser
    void post_delete_fail1() throws Exception {
        long postId = 1l;
        String JwtUserName = "태근";
        when(postService.deletePost(postId, JwtUserName))
                .thenThrow(new AppException(ErrorCode.INVALID_TOKEN));

        mockMvc.perform(delete("/api/v1/posts/1")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }

    @Test
    @DisplayName("게시글 삭제 실패(2) 작성자 불일치")
    @WithMockUser
    void post_delete_fail2() throws Exception {

        when(postService.deletePost(anyLong(), any()))
                .thenThrow(new AppException(ErrorCode.INVALID_PERMISSION));

        mockMvc.perform(delete("/api/v1/posts/1")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(ErrorCode.INVALID_PERMISSION.getHttpStatus().value()));
    }

    @Test
    @DisplayName("게시글 삭제 실패(3) 데이터베이스 에러")
    @WithMockUser
    void post_delete_fail3() throws Exception {

        when(postService.deletePost(anyLong(), any()))
                .thenThrow(new AppException(ErrorCode.DATABASE_ERROR));

        mockMvc.perform(delete("/api/v1/posts/1")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(ErrorCode.DATABASE_ERROR.getHttpStatus().value()));
    }


    @Test
    @DisplayName("게시글 목록 조회 성공")
    @WithMockUser
    void post_getList_success() throws Exception {

        List<PostDto> postDtoList = List.of(PostDto.builder()
                .title("제목")
                .createdAt(LocalDateTime.now())
                .build());

        Page<PostDto> response = new PageImpl<>(postDtoList);

        given(postService.getAllPost(any())).willReturn(response);

        mockMvc.perform(get("/api/v1/posts")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result.content").exists())
                .andExpect(jsonPath("$['result']['content'][0]['title']").exists())
                .andExpect(jsonPath("$['result']['content'][0]['createdAt']").exists())
                .andDo(print());

    }


    @Test
    @DisplayName("댓글 작성 성공")
    @WithMockUser
    void comment_write_success() throws Exception {
        CommentCreateRequest commentCreateRequest = new CommentCreateRequest("댓글");
        User jwtUser = UserFixture.get("댓글작성자", "1234");
        Post post = PostFixture.get("게시글작성자", "1234");

        CommentDto commentDto = CommentDto.builder()
                .comment("댓글")
                .commentId(1l)
                .user(jwtUser)
                .post(post)
                .createdAt(LocalDateTime.now())
                .build();

        when(postService.writeComment(any(), any(), any()))
                .thenReturn(commentDto);

        mockMvc.perform(post("/api/v1/posts/1/comments")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(commentCreateRequest)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result.id").exists())
                .andExpect(jsonPath("$.result.postId").exists())
                .andExpect(jsonPath("$.result.comment").exists())
                .andExpect(jsonPath("$.result.userName").exists())
                .andExpect(jsonPath("$.result.createdAt").exists());

    }

    @Test
    @DisplayName("댓글 작성 실패(1) 로그인을 하지 않은 경우")
    @WithAnonymousUser
    void comment_write_fail1() throws Exception {
        CommentCreateRequest commentCreateRequest = new CommentCreateRequest("댓글");

        when(postService.writeComment(any(), any(), any()))
                .thenThrow(new AppException(ErrorCode.INVALID_TOKEN));

        mockMvc.perform(post("/api/v1/posts/1/comments")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(commentCreateRequest)))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }

    @Test
    @DisplayName("댓글 작성 실패(2) 게시글이 존재하지 않는 경우")
    @WithMockUser
    void comment_write_fail2() throws Exception {
        CommentCreateRequest commentCreateRequest = new CommentCreateRequest("댓글");

        when(postService.writeComment(any(), any(), any()))
                .thenThrow(new AppException(ErrorCode.POST_NOT_FOUND));

        mockMvc.perform(post("/api/v1/posts/1/comments")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(commentCreateRequest)))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    @DisplayName("댓글 수정 성공")
    @WithMockUser
    void comment_modify_success() throws Exception {
        CommentModifyRequest commentModifyRequest = new CommentModifyRequest("수정된댓글");
        User jwtUser = UserFixture.get("댓글작성자", "1234");
        Post post = PostFixture.get("게시글작성자", "1234");

        CommentDto commentDto = CommentDto.builder()
                .commentId(1l)
                .comment(commentModifyRequest.getComment())
                .createdAt(LocalDateTime.now())
                .lastModifiedAt(LocalDateTime.now())
                .createdAt(LocalDateTime.now())
                .user(jwtUser)
                .post(post)
                .build();

        when(postService.modifyComment(anyLong(), anyLong(), any(), any()))
                .thenReturn(commentDto);


        mockMvc.perform(put("/api/v1/posts/1/comments/1")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(commentModifyRequest)))
                .andDo(print())
                .andExpect(jsonPath("$.result.id").exists())
                .andExpect(jsonPath("$.result.comment").exists())
                .andExpect(jsonPath("$.result.userName").exists())
                .andExpect(jsonPath("$.result.createdAt").exists())
                .andExpect(jsonPath("$.result.lastModifiedAt").exists())
                .andExpect(jsonPath("$.result.postId").exists())
                .andExpect(status().isOk());

    }

    @Test
    @DisplayName("댓글 수정 실패(1) 인증 실패")
    @WithAnonymousUser
    void comment_modify_fail1() throws Exception {
        CommentModifyRequest commentModifyRequest = new CommentModifyRequest("수정된댓글");

        when(postService.modifyComment(anyLong(), anyLong(), any(), any()))
                .thenThrow(new AppException(ErrorCode.INVALID_TOKEN));

        mockMvc.perform(put("/api/v1/posts/1/comments/1")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(commentModifyRequest)))
                .andDo(print())
                .andExpect(status().isUnauthorized());

    }

    @Test
    @DisplayName("댓글 수정 실패(2) Post 없는 경우")
    @WithMockUser
    void comment_modify_fail2() throws Exception {
        CommentModifyRequest commentModifyRequest = new CommentModifyRequest("수정된댓글");

        when(postService.modifyComment(anyLong(), anyLong(), any(), any()))
                .thenThrow(new AppException(ErrorCode.POST_NOT_FOUND));

        mockMvc.perform(put("/api/v1/posts/1/comments/1")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(commentModifyRequest)))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    @DisplayName("댓글 수정 실패(3) 작성자 불일치")
    @WithMockUser
    void comment_modify_fail3() throws Exception {
        CommentModifyRequest commentModifyRequest = new CommentModifyRequest("수정된댓글");

        when(postService.modifyComment(anyLong(), anyLong(), any(), any()))
                .thenThrow(new AppException(ErrorCode.INVALID_PERMISSION));

        mockMvc.perform(put("/api/v1/posts/1/comments/1")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(commentModifyRequest)))
                .andDo(print())
                .andExpect(status().is(ErrorCode.INVALID_PERMISSION.getHttpStatus().value()));
    }

    @Test
    @DisplayName("댓글 수정 실패(4) 데이터베이스 에러")
    @WithMockUser
    void comment_modify_fail4() throws Exception {
        CommentModifyRequest commentModifyRequest = new CommentModifyRequest("수정된댓글");

        when(postService.modifyComment(anyLong(), anyLong(), any(), any()))
                .thenThrow(new AppException(ErrorCode.DATABASE_ERROR));

        mockMvc.perform(put("/api/v1/posts/1/comments/1")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(commentModifyRequest)))
                .andDo(print())
                .andExpect(status().is(ErrorCode.DATABASE_ERROR.getHttpStatus().value()));
    }

    @Test
    @DisplayName("댓글 삭제 성공")
    @WithMockUser
    void comment_delete_success() throws Exception {
        CommentDto commentDto = CommentDto.builder()
                .commentId(1l)
                .build();

        when(postService.deleteComment(anyLong(), anyLong(), any()))
                .thenReturn(commentDto);

        mockMvc.perform(delete("/api/v1/posts/1/comments/1")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(jsonPath("$.result.message").exists())
                .andExpect(jsonPath("$.result.id").exists())
                .andExpect(status().isOk());

    }

    @Test
    @DisplayName("댓글 삭제 실패(1) 인증 실패")
    @WithAnonymousUser
    void comment_delete_fail1() throws Exception {

        when(postService.deleteComment(anyLong(), anyLong(), any()))
                .thenThrow(new AppException(ErrorCode.INVALID_TOKEN));

        mockMvc.perform(delete("/api/v1/posts/1/comments/1")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }

    @Test
    @DisplayName("댓글 삭제 실패(2) Post없는 경우")
    @WithMockUser
    void comment_delete_fail2() throws Exception {

        when(postService.deleteComment(anyLong(), anyLong(), any()))
                .thenThrow(new AppException(ErrorCode.POST_NOT_FOUND));

        mockMvc.perform(delete("/api/v1/posts/1/comments/1")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    @DisplayName("댓글 삭제 실패(3) 작성자 불일치")
    @WithMockUser
    void comment_delete_fail3() throws Exception {

        when(postService.deleteComment(anyLong(), anyLong(), any()))
                .thenThrow(new AppException(ErrorCode.INVALID_PERMISSION));

        mockMvc.perform(delete("/api/v1/posts/1/comments/1")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(ErrorCode.INVALID_PERMISSION.getHttpStatus().value()));
    }

    @Test
    @DisplayName("댓글 삭제 실패(4) 데이터베이스 에러")
    @WithMockUser
    void comment_delete_fail4() throws Exception {

        when(postService.deleteComment(anyLong(), anyLong(), any()))
                .thenThrow(new AppException(ErrorCode.DATABASE_ERROR));

        mockMvc.perform(delete("/api/v1/posts/1/comments/1")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is(ErrorCode.DATABASE_ERROR.getHttpStatus().value()));
    }

    @Test
    @DisplayName("댓글 조회 성공")
    @WithMockUser
    void comment_get_success() throws Exception {

        List<CommentListResponse> commentListResponses = List.of(CommentListResponse.builder()
                .comment("댓글")
                .postId(1l)
                .createdAt(LocalDateTime.now())
                .build());

        Page<CommentListResponse> response = new PageImpl<>(commentListResponses);

        given(postService.getAllComment(any(), anyLong())).willReturn(response);

        mockMvc.perform(get("/api/v1/posts/1/comments")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$['result']['content'][0]['comment']").exists())
                .andExpect(jsonPath("$['result']['content'][0]['postId']").exists())
                .andDo(print());
    }

    @Test
    @DisplayName("댓글 조회 실패 인증실패")
    @WithAnonymousUser
    void comment_get_fail() throws Exception {

        given(postService.getAllComment(any(), anyLong())).willThrow(new AppException(ErrorCode.INVALID_TOKEN));

        mockMvc.perform(get("/api/v1/posts/1/comments")
                        .with(csrf()))
                .andExpect(status().isUnauthorized())
                .andDo(print());
    }

    @Test
    @DisplayName("좋아요 누르기 성공")
    @WithMockUser
    void like_success() throws Exception {
        LikeDto likeDto = LikeDto.builder()
                .activation(true)
                .build();

        when(postService.likePost(any(), any()))
                .thenReturn(likeDto);

        mockMvc.perform(post("/api/v1/posts/1/likes")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result.result").value("좋아요를 눌렀습니다"));
    }

    @Test
    @DisplayName("좋아요 누르기 실패(1) 로그인 하지 않은 경우")
    @WithAnonymousUser
    void like_fail1() throws Exception {

        when(postService.likePost(any(), any()))
                .thenReturn(new LikeDto());

        mockMvc.perform(post("/api/v1/posts/1/likes")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }

    @Test
    @DisplayName("좋아요 누르기 실패(2) 해당 Post가 없는 경우")
    @WithMockUser
    void like_fail2() throws Exception {

        when(postService.likePost(any(), any()))
                .thenThrow(new AppException(ErrorCode.POST_NOT_FOUND));

        mockMvc.perform(post("/api/v1/posts/1/likes")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    @DisplayName("마이피드 조회 성공")
    @WithMockUser
    void my_feed_get_success() throws Exception {

        List<FeedResponse> feedResponseList = List.of(FeedResponse.builder()
                .title("제목")
                .createdAt(LocalDateTime.now())
                .build());


        Page<FeedResponse> response = new PageImpl<>(feedResponseList);

        given(postService.getMyPost(any(), any())).willReturn(response);

        mockMvc.perform(get("/api/v1/posts/my")
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result.content").exists())
                .andExpect(jsonPath("$.result.pageable").exists())
                .andExpect(jsonPath("$['result']['content'][0]['title']").exists())
                .andExpect(jsonPath("$['result']['content'][0]['createdAt']").exists())
                .andDo(print());
    }

    @Test
    @DisplayName("마이피드 조회 실패 - 인증실패")
    @WithAnonymousUser
    void my_feed_get_fail() throws Exception {

        given(postService.getMyPost(any(), any())).willThrow(new AppException(ErrorCode.INVALID_TOKEN));

        mockMvc.perform(get("/api/v1/posts/my")
                        .with(csrf()))
                .andExpect(status().isUnauthorized())
                .andDo(print());
    }
}